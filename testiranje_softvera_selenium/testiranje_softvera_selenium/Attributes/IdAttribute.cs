﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testiranje_softvera_selenium.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    internal class IdAttribute : Attribute
    {
        public string Id { get; set; }

        public bool IsComboBox { get; set; }
        public bool IsCheckBox { get; set; }

        public IdAttribute(string id)
        {
            this.Id = id;
        }
    }
}

﻿using OpenQA.Selenium;
using System.Collections.Generic;
using testiranje_softvera_selenium.Models;


namespace testiranje_softvera_selenium.API
{
    public class MonthlyAccountsElectricEnergyTestCase
    {
        public static void Execute()
        {
            if(IsExists())
            {
                UpdateMeasuringPoint();
            }
            else
            {
                AddMeasuringPoint();
                Helper.Wait(5);
            }            
            
            UpdateReceipt();
            Helper.Wait(3);
                       
            
        }

        private static void AddMeasuringPoint()
        {
            Helper.ClickOnLinkByLinkText("Unos novog mernog mesta");

            Helper.SelectRandomItemInComboBox("TipKategorijePotrosnjeId");

            MeasuringPoint mernoMesto = new MeasuringPoint()
            {
                BrojMernogMesta = 55,
                OpisMernogMesta = "Opis mernog mesta broj 55",
                TipKategorijePotrosnje = 2,
                ObracunaskaSnaga = true,
                PrekomernoPreuzetaSnaga = true,
                AktivnaEnergija_JT = true,
                AktivnaEnergija_VT = true,
                AktivnaEnergija_NT = true,
                ReaktivnaEnergija  = true,
                PrekomernaReaktivnaEnergija = true
            };

            Helper.PopulateForm(mernoMesto);

            Helper.ClickOnLinkByCss(".btn.btn-success");            
        }

        private static void UpdateMeasuringPoint()
        {
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-pencil");

            MeasuringPoint mernoMesto = new MeasuringPoint()
            {
                BrojMernogMesta = 555,
                OpisMernogMesta = "Opis mernog mesta broj 55 izmena 555",
                TipKategorijePotrosnje = 6,
                ObracunaskaSnaga = true,
                PrekomernoPreuzetaSnaga = false,
                AktivnaEnergija_JT = true,
                AktivnaEnergija_VT = false,
                AktivnaEnergija_NT = true,
                ReaktivnaEnergija = false,
                PrekomernaReaktivnaEnergija = true
            };

            Helper.PopulateForm(mernoMesto);

            Helper.ClickOnLinkByCss(".btn.btn-success");
        }

        private static void UpdateReceipt()
        {
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-stats");
            Helper.ClickOnLinkByCss(".btn.btn-info");

            Helper.RandomizeNumberFields(1, 100);

            Helper.ClickOnLinkByCss(".btn.btn-success");
            Helper.ClickOnLinkByCss(".btn.btn-default.pull-right");
        }

        public static void DeleteMeasuringPoint()
        {
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-trash");
            Helper.ClickOnLinkByCss(".btn.btn-danger");            
        }

        public static bool IsExists()
        {
            IList<IWebElement> numberPlacesTables = BrowserFactory.BrowserFactory.Driver.FindElements(By.ClassName("table"));

            if (numberPlacesTables.Count == 0)
                return false;

            foreach (var place in numberPlacesTables)
            {
                IList<IWebElement> tableData = place.FindElements(By.CssSelector("td"));

                if (tableData.Count != 0)
                    return true;
            }
            return false;
        }

        public static void InvalidInputsAndBugs()
        {
            if(IsExists())
            {
                Helper.ClickOnLinkByCss(".glyphicon.glyphicon-stats");
                Helper.ClickOnLinkByCss(".btn.btn-info");

                IWebElement field = BrowserFactory.BrowserFactory.Driver.FindElement(By.Name("[0].ObracunskaSnaga"));
                field.Clear();
                field.SendKeys("text");
                Helper.Wait(2);
                Helper.ClickOnLinkByCss(".btn.btn-success");


                if (Helper.IsErrorPage())
                {
                    Helper.Wait(2);
                    BrowserFactory.BrowserFactory.Driver.Navigate().Back();
                    Helper.ClickOnLinkByCss(".btn.btn-default");
                    Helper.ClickOnLinkByCss(".btn.btn-default.pull-right");                    
                }
            }
        }
    }
}

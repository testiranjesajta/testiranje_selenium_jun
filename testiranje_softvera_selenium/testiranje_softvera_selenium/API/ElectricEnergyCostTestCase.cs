﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testiranje_softvera_selenium.API
{
    public class ElectricEnergyCostTestCase
    {
        public static void Execute()
        {
            Helper.RandomizeNumberFieldsWithSum(1, 100, 100);

            Helper.SelectRandomItemInComboBox("TipNacinPlacanjaId");

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");
        }
    }
}

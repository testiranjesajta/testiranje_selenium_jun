﻿using testiranje_softvera_selenium.Models;

namespace testiranje_softvera_selenium.API
{
    public class SystemMaintenanceTestCase
    {
        public static void Execute()
        {
            Helper.SelectItemInComboBoxByIndex("TipPlacanjeTroskovaOdrzavanje", 0);

            Helper.RandomizeNumberFields(100, 500);

            SystemMaintenance systemMaintenance = new SystemMaintenance()
            {
                KoVrsiOdrzavanjeJavnogOsvetljenja = 3,
                NazivPreduzecaKojeVrsiOdrzavanje = "JKP Komnis",
                NacinPlacanjaUslugeOdrzavanja = 0,
                DinamikaOdrzavanjaJavnogOsvetljenja = 2,
                OcenaOdrzavanjaSistemaJavnogOsvetljenja = 1
            };

            Helper.PopulateForm(systemMaintenance);
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");
        }

        public static void ShowBugs()
        {
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-pencil");

            Helper.InsertRandomValueInFieldByName("ProcenatNeispravnihSijalica");
            Helper.Wait(2);

            Helper.SelectItemInComboBoxByIndex("TipPlacanjeTroskovaOdrzavanje", 0);
            Helper.Wait(2);

            Helper.SelectItemInComboBoxByIndex("TipPlacanjeTroskovaOdrzavanje", 1);
            Helper.HighlightDomElement("ProcenatNeispravnihSijalica");
            Helper.AddProblemDescriptionInParentElementById("TipPlacanjeTroskovaOdrzavanje", "Promenom sa druge mogucnosti na prvu, resetuje se vrednost ispod i onemogucava njen unos!");
            Helper.Wait(2);  

            Helper.AddProblemDescriptionInParentElementByName("[0].Iznos", "Pokusaj unosa procenata tako da zbir ne bude 100%");

            Helper.Wait(2);

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");

            Helper.Wait(1);

            Helper.AddProblemDescriptionInParentElementByClassName("table table-hover", "Uspesno snimljene vrednosti za procente koje su u zbiru vece od 100, nazalost!");
            
            Helper.Wait(3);
        }
    }
}

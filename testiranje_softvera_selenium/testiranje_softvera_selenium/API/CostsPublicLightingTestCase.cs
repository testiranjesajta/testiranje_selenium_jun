﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testiranje_softvera_selenium.API
{
    public class CostsPublicLightingTestCase
    {
        public static void Execute()
        {
            Helper.RandomizeNumberFields(1,100);

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");            

            Helper.AddProblemDescriptionInParentElementByClassName("table table-hover", "Broj dana za mesec, broj sati i procente ograniciti na interval 1-31 u zavisnosti od meseca, 0-24h i 0-100%!");            
        }
    }
}

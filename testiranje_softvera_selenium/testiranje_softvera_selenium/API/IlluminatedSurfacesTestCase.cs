﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testiranje_softvera_selenium.API
{
    public class IlluminatedSurfacesTestCase
    {
        public static void Execute()
        {
            Helper.RandomizeNumberFields(1, 100);

            Helper.ClickOnLinkByCss(".btn.btn-success");
            Helper.Wait(2);

            Helper.AddProblemDescriptionInParentElementByClassName("table table-hover", "Duzina osvetljenog dela ulice ne moze biti veca od ukupne duzine ulice!");
        }
    }
}

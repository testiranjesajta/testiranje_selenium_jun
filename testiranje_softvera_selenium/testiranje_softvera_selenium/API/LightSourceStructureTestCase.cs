﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testiranje_softvera_selenium.API
{
    public class LightSourceStructureTestCase
    {
        public static void Execute()
        {
            Helper.RandomizeNumberFields(1, 10);

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");

            Helper.Wait(3);

            //Brisanje LED
            Helper.ClickOnLinkByCssWithAlert(".glyphicon.glyphicon-trash");
       
            //Dodavanje LED
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-list-alt");

            if(IsExistsLED())
            {
                Helper.SelectItemInComboBoxByIndex("TipIzvoraSvetlostiSnagaId", 1);
                Helper.ClickOnLinkByCss(".btn.btn-success");
            }             
        }

        private static bool IsExistsLED()
        {
            IList<IWebElement> led = BrowserFactory.BrowserFactory.Driver.FindElements(By.ClassName("table"));

            if (led.Count == 0)
                return false;

            foreach (var l in led)
            {
                IList<IWebElement> tableData = l.FindElements(By.CssSelector("td"));

                if (tableData.Count != 0)
                    return true;
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testiranje_softvera_selenium.API
{
    public class ReportsTestCase
    {
        public static void Execute()
        {
            Helper.SelectItemInComboBoxByValue("CategoryName", "Osnovne informacije");
            Helper.ClickOnLinkByCss(".btn.btn-success");

            Helper.SelectItemInComboBoxByValue("CategoryName", "Struktura izvora svetlosti");
            Helper.SelectItemInComboBoxByValue("TypeOfLightSourceReport", "ukupno");
            Helper.ClickOnLinkByCss(".btn.btn-success");

            Helper.SelectItemInComboBoxByValue("CategoryName", "Vlasništvo nad stubovima javnog osvetljenja");
            Helper.ClickOnLinkByCss(".btn.btn-success");

            Helper.SelectItemInComboBoxByValue("CategoryName", "Mesečni računi za električnu energiju");
            Helper.SelectItemInComboBoxByValue("TypeOfElectricEnergyBills", "ukupno");
            Helper.ClickOnLinkByCss(".btn.btn-success");
        }
    }
}

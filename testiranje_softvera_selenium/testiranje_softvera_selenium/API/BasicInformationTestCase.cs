﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testiranje_softvera_selenium.Extensions;
using testiranje_softvera_selenium.Models;
using testiranje_softvera_selenium.PageObjects;

namespace testiranje_softvera_selenium.API
{
    public class BasicInformationTestCase
    {
        public static void Execute()
        {
            BasicInformation basicInformation = new BasicInformation()
            {
                UgovoreniProcenatIspravnihSijalica = 80,
                UgovoreniBrojRadnihSati = 45,
                Specificnosti = "Ostale specifičnosti ugovora",
                TipRegulacijaUkljucenjaIskljucenja = 1,
                TipRegulacijaFotometrijskihParametara = 1,
                OpisRegulacijaFotometrijskihParametara = "Opis regulacija fotometrijskih parametara",
                RekonstrukcijaSistema = true,
                OpisRekonstrukcije = "Rekonstrukcija je uradjenja!"
            };

            Helper.PopulateForm(basicInformation);

            Helper.Wait(2);

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");        
        }

        public static void InvalidInputsAndBugs()
        {
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-pencil");

            BasicInformation basicInformation = new BasicInformation()
            {
                UgovoreniProcenatIspravnihSijalica = 54,
                UgovoreniBrojRadnihSati = -10,
                Specificnosti = "Ostale specifičnosti ugovora",
                TipRegulacijaUkljucenjaIskljucenja = 1,
                TipRegulacijaFotometrijskihParametara = 1,
                OpisRegulacijaFotometrijskihParametara = "Opis regulacija fotometrijskih parametara",
                RekonstrukcijaSistema = true,
                OpisRekonstrukcije = "Polje je obavezno, a ne moze se editovati ukoliko checkbox iznad nije selektovan. Dakle, ili omoguciti da polje ne bude obavezno ili dati mogucnost unosa u slucaju da checkbox iznad nije cekiran"
            };

            Helper.PopulateForm(basicInformation);            

            Helper.AddProblemDescriptionInParentElementById("UgovoreniBrojRadnihSati", "Dozvoljava negativne brojeve, sto nema smisla za broj sati!");
            Helper.Wait(3);

            Helper.HighlightDomElement("OpisRekonstrukcije");
            Helper.Wait(3);            

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");
            Helper.Wait(3);

            TestImportScriptsAndIntOverflow("Specificnosti");
        }

        private static void TestImportScriptsAndIntOverflow(string idElement)
        {
            IEnumerable<string> strings = new string[] { "script", "<script", "<script>", "Тест" };

            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-pencil");            

            foreach (var text in strings)
            {
                IWebElement textField = BrowserFactory.BrowserFactory.Driver.FindElement(By.Id(idElement));

                textField.EnterText(text, "");

                Helper.HighlightDomElement(idElement);

                Helper.Wait(3);

                Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");

                if (Helper.IsErrorPage())
                {
                    Helper.Wait(3);
                    BrowserFactory.BrowserFactory.Driver.Navigate().Back();
                }
                    
                else
                {
                    Helper.ClickOnLinkByCss(".glyphicon.glyphicon-pencil");
                }
            }

            IWebElement textField1 = BrowserFactory.BrowserFactory.Driver.FindElement(By.Id("UgovoreniProcenatIspravnihSijalica"));
            textField1.EnterText(Int64.MaxValue.ToString(), "");
            Helper.HighlightDomElement("UgovoreniProcenatIspravnihSijalica");
            Helper.Wait(2);
            Helper.ClickOnLinkByCss(".glyphicon.glyphicon-ok");

            if (Helper.IsErrorPage())
            {
                Helper.Wait(3);
                BrowserFactory.BrowserFactory.Driver.Navigate().Back();                
            }               
        }
    }
}

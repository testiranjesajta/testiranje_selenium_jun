﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using testiranje_softvera_selenium.Attributes;
using testiranje_softvera_selenium.Extensions;

namespace testiranje_softvera_selenium
{
    public static class Helper
    {
        public static void RandomizeNumberFields(int lower, int upper)
        {
            var numberFields = BrowserFactory.BrowserFactory.Driver.FindElements(By.CssSelector(@"input[type='number']"));
            //var decimalFields = webDriver.FindElements(By.CssSelector("input[value='0.00']"));
            var textFieldsRequiringNumbers = BrowserFactory.BrowserFactory.Driver.FindElements(By.CssSelector(@"input[type='text'][data-val-number]"));

            Random randomizer = new Random();
            foreach (var webElement in numberFields/*.Union(decimalFields)*/.Union(textFieldsRequiringNumbers))
            {
                webElement.Clear();
                webElement.SendKeys(randomizer.Next(lower, upper).ToString());
            }
        }

        public static void InsertRandomValueInFieldByName(string name)
        {
            var field = BrowserFactory.BrowserFactory.Driver.FindElement(By.Name(name));

            field.Clear();
            field.SendKeys(new Random().Next(1, 1000).ToString());
        }

        public static void RandomizeNumberFieldsWithSum(int lower, int upper, int sum)
        {
            var numberFields = BrowserFactory.BrowserFactory.Driver.FindElements(By.CssSelector(@"input[type='number']"));
            var textFieldsRequiringNumbers = BrowserFactory.BrowserFactory.Driver.FindElements(By.CssSelector(@"input[type='text'][data-val-number]"));

            Random generator = new Random();

            int values = numberFields.Count + textFieldsRequiringNumbers.Count;
            var numberList = new int[values];

            for (var index = 0; index < values - 1; index++)
            {
                var rest = numberList.Length - (index + 1);

                var restMinimum = lower * rest;
                var restMaximum = upper * rest;

                lower = (int)Math.Max(lower, sum - restMaximum);
                upper = (int)Math.Min(upper, sum - restMinimum);

                var newRandomValue = generator.Next(lower, upper);
                numberList[index] = newRandomValue;
                sum -= newRandomValue;
            }

            numberList[values - 1] = sum;

            int i = 0;
            foreach (var webElement in numberFields.Union(textFieldsRequiringNumbers))
            {
                webElement.Clear();
                webElement.SendKeys(numberList[i].ToString());
                i++;
            }
        }

        public static void SelectRandomItemInComboBox(string id)
        {
            try
            {
                var wait = new WebDriverWait(BrowserFactory.BrowserFactory.Driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.Id(id)));

                var selectElement = new SelectElement(wait);
                int numberOfChoices = selectElement.AllSelectedOptions.Count;
                selectElement.SelectByIndex(new Random().Next(1, numberOfChoices + 1));
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public static void SelectItemInComboBoxByIndex(string id, int index)
        {
            try
            {
                var selectElement = new SelectElement(BrowserFactory.BrowserFactory.Driver.FindElement(By.Id(id)));
                
                if(selectElement != null)
                    selectElement.SelectByIndex(index);
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public static void SelectItemInComboBoxByValue(string id, string value)
        {
            try
            {
                var selectElement = new SelectElement(BrowserFactory.BrowserFactory.Driver.FindElement(By.Id(id)));

                if (selectElement != null)
                    selectElement.SelectByValue(value);
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public static void ClickOnLinkByCss(string cssSelector)
        {
            try
            {
                var wait = new WebDriverWait(BrowserFactory.BrowserFactory.Driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.CssSelector(cssSelector)));

                wait.Click();
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public static void ClickOnLinkByLinkText(string linkText)
        {
            try
            {
                var wait = new WebDriverWait(BrowserFactory.BrowserFactory.Driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.LinkText(linkText)));

                wait.Click();
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public static void HandleAlert(IWebDriver driver, WebDriverWait wait)
        {
            if (wait == null)
            {
                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            }

            try
            {
                IAlert alert = wait.Until(drv => {
                    try
                    {
                        return drv.SwitchTo().Alert();
                    }
                    catch (NoAlertPresentException)
                    {
                        return null;
                    }
                });
                alert.Accept();
            }
            catch (WebDriverTimeoutException) { /* Ignore */ }
        }


        public static void ClickOnLinkByCssWithAlert(string cssSelector)
        {
            try
            {
                var element = BrowserFactory.BrowserFactory.Driver.FindElement(By.CssSelector(cssSelector));
                element.Click();
                Helper.HandleAlert(BrowserFactory.BrowserFactory.Driver, new WebDriverWait(BrowserFactory.BrowserFactory.Driver, new TimeSpan(0, 0, 10)));
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public static void Wait(int seconds)
        {
            BrowserFactory.BrowserFactory.Driver.Wait(seconds * 1000, 500);
        }

        public static void HighlightDomElement(string id)
        {
            IJavaScriptExecutor js = BrowserFactory.BrowserFactory.Driver as IJavaScriptExecutor;
            js.ExecuteScript($"document.getElementById('{id}').style='background: red; color:black;'");
        }

        public static void AddProblemDescriptionInParentElementById(string id, string description)
        {
            IJavaScriptExecutor js = BrowserFactory.BrowserFactory.Driver as IJavaScriptExecutor;
            js.ExecuteScript($"var newDiv = document.createElement('div'); var newTextNode = document.createTextNode('{description}'); newDiv.style='background: red; color: white'; newDiv.appendChild(newTextNode); document.getElementById('{id}').parentElement.appendChild(newDiv);");
        }

        
        public static void AddProblemDescriptionInParentElementByName(string name, string description)
        {
            IJavaScriptExecutor js = BrowserFactory.BrowserFactory.Driver as IJavaScriptExecutor;
            js.ExecuteScript($"var newDiv = document.createElement('div'); var newTextNode = document.createTextNode('{description}'); newDiv.style='background: red; color: white'; newDiv.appendChild(newTextNode); document.getElementsByName('{name}')[0].parentElement.appendChild(newDiv);");
        }


        public static void AddProblemDescriptionInParentElementByClassName(string className, string description)
        {
            IJavaScriptExecutor js = BrowserFactory.BrowserFactory.Driver as IJavaScriptExecutor;
            js.ExecuteScript($"var newDiv = document.createElement('div'); var newTextNode = document.createTextNode('{description}'); newDiv.style='background: red; color: white'; newDiv.appendChild(newTextNode); document.getElementsByClassName('{className}')[0].parentElement.appendChild(newDiv);");
        }

        public static void PopulateForm(object data)
        {
            foreach (PropertyInfo propertyInfo in data.GetType().GetProperties())
            {
                var attribute = propertyInfo.GetCustomAttribute<IdAttribute>();

                if(attribute != null)
                {
                    IWebElement inputField = BrowserFactory.BrowserFactory.Driver.FindElement(By.Id(attribute.Id));
                    if (attribute.IsComboBox)
                    {
                        var selectElement = new SelectElement(inputField);
                        selectElement.SelectByIndex((int)propertyInfo.GetValue(data));
                    }
                    else if (attribute.IsCheckBox)
                    {
                        if (inputField.Selected == false)
                            inputField.Click();
                    }
                    else
                    {
                        inputField.Clear();
                        inputField.SendKeys(propertyInfo.GetValue(data)?.ToString() ?? string.Empty);
                    }
                }                
            }
        }

        public static bool IsErrorPage()
        {
            try
            {
                BrowserFactory.BrowserFactory.Driver.FindElement(By.ClassName("error"));
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using testiranje_softvera_selenium.API;
using testiranje_softvera_selenium.PageObjects;

namespace testiranje_softvera_selenium
{
    class Program
    {
        static void Main(string[] args)
        {
            //login stranica
            BrowserFactory.BrowserFactory.InitBrowser("Chrome");
            BrowserFactory.BrowserFactory.LoadApplication("http://160.99.32.110/");

            //---------------------------------------------------------------------------------------------------

            //logovanje na web aplikaciju
            Page.Login.LogInToApplication("nikolic.stefan@elfak.rs", "nikolic.stefan@elfak.rs");

            //---------------------------------------------------------------------------------------------------

            //opstina planovi
            Page.OpstinaPlanovi.ClickToOpenPlanButton();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> OSNOVNE INFORMACIJE
            Page.MeniLevo.ClickToItemInLeftMenu("Osnovne informacije");            

            BasicInformationTestCase.Execute(); 
            Helper.Wait(4);
            BasicInformationTestCase.InvalidInputsAndBugs();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> VLASNISTVO NAD STUBOVIMA JAVNOG OSVETLJENJA
            Page.MeniLevo.ClickToItemInLeftMenu("Vlasništvo nad stubovima javnog osvetljenja");

            OwnerPillarsTestCase.Execute();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> VLASNISTVO NAD SVETILJKAMA I SIJALICAMA JAVNOG OSVETLJENJA
            Page.MeniLevo.ClickToItemInLeftMenu("Vlasništvo nad svetiljkama i sijalicama javnog osvetljenja");
            OwnerBulbsTestCase.Execute();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> TROSKOVI ZA ELEKTRICNU ENERGIJU
            Page.MeniLevo.ClickToItemInLeftMenu("Troškovi za električnu energiju");
            ElectricEnergyCostTestCase.Execute();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> ODRZAVANJE SISTEMA
            Page.MeniLevo.ClickToItemInLeftMenu("Održavanje sistema");

            SystemMaintenanceTestCase.Execute();
            Helper.Wait(4);
            SystemMaintenanceTestCase.ShowBugs();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> STRUKTURA IZVORA SVETLOSTI
            Page.MeniLevo.ClickToItemInLeftMenu("Struktura izvora svetlosti");

            LightSourceStructureTestCase.Execute();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> OSVETLJENE POVRSINE
            Page.MeniLevo.ClickToItemInLeftMenu("Osvetljene površine");
            Helper.Wait(4);

            IlluminatedSurfacesTestCase.Execute();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> MESECNI RACUNI ZA ELEKTRICNU ENERGIJU
            Page.MeniLevo.ClickToItemInLeftMenu("Mesečni računi za električnu energiju");

            MonthlyAccountsElectricEnergyTestCase.Execute();
            MonthlyAccountsElectricEnergyTestCase.InvalidInputsAndBugs();
            Helper.Wait(4);
            MonthlyAccountsElectricEnergyTestCase.DeleteMeasuringPoint();

            //---------------------------------------------------------------------------------------------------

            //levi meni -> TROSKOVI ZA JAVNO OSVETLJENJE
            Page.MeniLevo.ClickToItemInLeftMenu("Troškovi za javno osvetljenje");

            CostsPublicLightingTestCase.Execute();
            Helper.Wait(4);

            //---------------------------------------------------------------------------------------------------

            //levi meni -> IZVESTAJI
            Page.MeniLevo.ClickToItemInLeftMenu("Izveštaji");

            ReportsTestCase.Execute();

            //---------------------------------------------------------------------------------------------------

            //END
            BrowserFactory.BrowserFactory.CloseAllDrivers();
        }
    }
}

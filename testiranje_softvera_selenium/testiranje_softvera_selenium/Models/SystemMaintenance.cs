﻿using testiranje_softvera_selenium.Attributes;

namespace testiranje_softvera_selenium.Models
{
    public class SystemMaintenance
    {
        [Id("TipOdrzavanja", IsComboBox = true)]
        public int KoVrsiOdrzavanjeJavnogOsvetljenja { get; set; }

        [Id("NazivPreduzecaOdrzavanje")]
        public string NazivPreduzecaKojeVrsiOdrzavanje { get; set; }

        public int TroskoviOpstina { get; set; }

        public int TroskoviMesnaZajednica { get; set; }

        public int TroskoviGradjani { get; set; }

        [Id("TipPlacanjeTroskovaOdrzavanje", IsComboBox = true)]
        public int NacinPlacanjaUslugeOdrzavanja { get; set; }

        public int MaksimalniProcenatNeispravnihSijalica { get; set; }

        [Id("TipDinamikaOdrzavanja", IsComboBox = true)]
        public int DinamikaOdrzavanjaJavnogOsvetljenja { get; set; }

        [Id("TipOcenaOdrzavanja", IsComboBox = true)]
        public int OcenaOdrzavanjaSistemaJavnogOsvetljenja { get; set; }
    }
}

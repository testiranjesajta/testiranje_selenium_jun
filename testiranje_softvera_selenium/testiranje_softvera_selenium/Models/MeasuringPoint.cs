﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testiranje_softvera_selenium.Attributes;

namespace testiranje_softvera_selenium.Models
{
    public class MeasuringPoint
    {
        [Id("BrojMernogMesta")]
        public int BrojMernogMesta { get; set; }

        [Id("Opis")]
        public string OpisMernogMesta { get; set; }

        [Id("TipKategorijePotrosnjeId", IsComboBox = true)]
        public int TipKategorijePotrosnje { get; set; }

        [Id("ObracunskaSnaga", IsCheckBox = true)]
        public bool ObracunaskaSnaga { get; set; }

        [Id("PrekomernoPreuzetaSnaga", IsCheckBox = true)]
        public bool PrekomernoPreuzetaSnaga { get; set; }

        [Id("AktivnaEnergijaJT", IsCheckBox = true)]
        public bool AktivnaEnergija_JT { get; set; }

        [Id("AktivnaEnergijaVT", IsCheckBox = true)]
        public bool AktivnaEnergija_VT { get; set; }

        [Id("AktivnaEnergijaNT", IsCheckBox = true)]
        public bool AktivnaEnergija_NT { get; set; }

        [Id("ReaktivnaEnergija", IsCheckBox = true)]
        public bool ReaktivnaEnergija { get; set; }

        [Id("PrekomernaReaktivnaEnergija", IsCheckBox = true)]
        public bool PrekomernaReaktivnaEnergija { get; set; }
    }
}

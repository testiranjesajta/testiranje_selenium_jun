﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testiranje_softvera_selenium.Attributes;

namespace testiranje_softvera_selenium.Models
{
    public class BasicInformation
    {
        [Id("UgovoreniProcenatIspravnihSijalica")]
        public int UgovoreniProcenatIspravnihSijalica { get; set; }

        [Id("UgovoreniBrojRadnihSati")]
        public int UgovoreniBrojRadnihSati { get; set; }

        [Id("Specificnosti")]
        public string Specificnosti { get; set; }

        [Id("TipRegulacijaUkljucenjaIskljucenjaId", IsComboBox = true)]
        public int TipRegulacijaUkljucenjaIskljucenja { get; set; }


        [Id("TipRegulacijaFotometrijskihParametaraId", IsComboBox = true)]
        public int TipRegulacijaFotometrijskihParametara { get; set; }

        [Id("RekonstrukcijaSistema", IsCheckBox = true)]
        public bool RekonstrukcijaSistema { get; set; }

        [Id("OpisRegulacijaFotometrijskihParametara")]
        public string OpisRegulacijaFotometrijskihParametara { get; set; }

        [Id("OpisRekonstrukcije")]
        public string OpisRekonstrukcije { get; set; }
    }
}

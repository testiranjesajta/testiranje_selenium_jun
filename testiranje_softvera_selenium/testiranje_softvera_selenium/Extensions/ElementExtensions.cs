﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace testiranje_softvera_selenium.Extensions
{
    public static class ElementExtensions
    {
        public static void EnterText(this IWebElement element, string text, string elementName)
        {            
            element.Clear();
            element.SendKeys(text);
            Console.WriteLine(text + " entered in the " + elementName + " field.");
        }

        public static void ClickOnIt(this IWebElement element, string elementName)
        {
            var wait = new WebDriverWait(BrowserFactory.BrowserFactory.Driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementToBeClickable(element));

            element.Click();
            Console.WriteLine("Clicked on " + elementName);
        }
    }
}

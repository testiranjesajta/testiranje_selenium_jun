﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using testiranje_softvera_selenium.Extensions;

namespace testiranje_softvera_selenium.PageObjects
{
    public class LoginPage
    {
        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.Name, Using = "Password")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "button")]
        public IWebElement SubmitLogin { get; set; }

        [FindsBy(How = How.Id, Using = "loginLink")]
        public IWebElement OpenLogInForm { get; set; }

        public void LogInToApplication(string username, string password)
        {
            OpenLogInForm.ClickOnIt("OpenLogInForm");
            
            UserName.EnterText(username, "UserName");
            Password.EnterText(password, "Password");
            SubmitLogin.ClickOnIt("SubmitButton");
        }
    }
}

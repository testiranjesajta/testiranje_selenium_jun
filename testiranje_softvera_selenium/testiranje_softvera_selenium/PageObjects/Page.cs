﻿using OpenQA.Selenium.Support.PageObjects;


namespace testiranje_softvera_selenium.PageObjects
{
    public static class Page
    {
        public static T GetPage<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(BrowserFactory.BrowserFactory.Driver, page);
            return page;
        }
        public static LoginPage Login
        {
            get { return GetPage<LoginPage>(); }
        }

        public static MunicipalityPlans OpstinaPlanovi
        {
            get { return GetPage<MunicipalityPlans>(); }
        }

        public static LeftMenu MeniLevo
        {
            get { return GetPage<LeftMenu>(); }
        }
    }
}

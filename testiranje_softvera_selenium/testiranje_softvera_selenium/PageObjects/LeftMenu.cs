﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testiranje_softvera_selenium.Extensions;

namespace testiranje_softvera_selenium.PageObjects
{
    public class LeftMenu
    {
        [FindsBy(How = How.LinkText, Using = "Osnovne informacije")]
        public IWebElement OsnovneInformacijeButton { get; set; }

        [FindsBy(How = How.LinkText, Using = "Vlasništvo nad stubovima javnog osvetljenja")]
        public IWebElement VlasništvoNadStubovimaJavnogOsvetljenja { get; set; }

        [FindsBy(How = How.LinkText, Using = "Vlasništvo nad svetiljkama i sijalicama javnog osvetljenja")]
        public IWebElement VlasnistvoNadSvetiljkamaISijalicamaJavnogOsvetljenjaButton { get; set; }

        [FindsBy(How = How.LinkText, Using = "Troškovi za električnu energiju")]
        public IWebElement TroškoviZaElektricnuEnergiju { get; set; }

        [FindsBy(How = How.LinkText, Using = "Održavanje sistema")]
        public IWebElement OdrzavanjeSistema { get; set; }

        [FindsBy(How = How.LinkText, Using = "Struktura izvora svetlosti")]
        public IWebElement StrukturaIzvoraSvetlosti { get; set; }

        [FindsBy(How = How.LinkText, Using = "Osvetljene površine")]
        public IWebElement OsvetljenePovrsine { get; set; }

        [FindsBy(How = How.LinkText, Using = "Mesečni računi za električnu energiju")]
        public IWebElement MesecniRacuniZaElektricnuEnergiju { get; set; }

        [FindsBy(How = How.LinkText, Using = "Troškovi za javno osvetljenje")]
        public IWebElement TroskoviZaJavnoOsvetljenje { get; set; }
        
        [FindsBy(How = How.LinkText, Using = "Izveštaji")]
        public IWebElement Izvestaji { get; set; }

        [FindsBy(How = How.LinkText, Using = "Izmeni")]
        public IWebElement Izmeni { get; set; }

        public void ClickToItemInLeftMenu(string linkTextForButton)
        {
            switch (linkTextForButton)
            {
                case "Osnovne informacije":
                    {
                        OsnovneInformacijeButton.ClickOnIt("Osnovne informacije");                        

                        Izmeni.ClickOnIt("Izmeni osnovne informacije");

                        break;
                    }
                case "Vlasništvo nad stubovima javnog osvetljenja":
                    {
                        VlasništvoNadStubovimaJavnogOsvetljenja.ClickOnIt("Vlasništvo nad stubovima javnog osvetljenja");                        

                        Izmeni.ClickOnIt("Izmeni vlasništvo nad stubovima javnog osvetljenja");

                        break;
                    }
                case "Vlasništvo nad svetiljkama i sijalicama javnog osvetljenja":
                    {
                        VlasnistvoNadSvetiljkamaISijalicamaJavnogOsvetljenjaButton.ClickOnIt("Vlasništvo nad svetiljkama i sijalicama javnog osvetljenja");

                        Izmeni.ClickOnIt("Izmeni vlasništvo nad svetiljkama i sijalicama javnog osvetljenja");

                        break;
                    }
                case "Troškovi za električnu energiju":
                    {
                        TroškoviZaElektricnuEnergiju.ClickOnIt("Troškovi za električnu energiju");

                        Izmeni.ClickOnIt("Izmeni troškovi za električnu energiju");

                        break;
                    }
                case "Održavanje sistema":
                    {
                        OdrzavanjeSistema.ClickOnIt("Održavanje sistema");

                        Izmeni.ClickOnIt("Izmeni održavanje sistema");

                        break;
                    }
                case "Struktura izvora svetlosti":
                    {
                        StrukturaIzvoraSvetlosti.ClickOnIt("Struktura izvora svetlosti");

                        Izmeni.ClickOnIt("Izmeni struktura izvora svetlosti");

                        break;
                    }
                case "Osvetljene površine":
                    {
                        OsvetljenePovrsine.ClickOnIt("Osvetljene površine");

                        Izmeni.ClickOnIt("Izmeni osvetljene površine");

                        break;
                    }
                case "Mesečni računi za električnu energiju":
                    {
                        MesecniRacuniZaElektricnuEnergiju.ClickOnIt("Mesečni računi za električnu energiju");

                        break;
                    }
                case "Troškovi za javno osvetljenje":
                    {
                        TroskoviZaJavnoOsvetljenje.ClickOnIt("Troškovi za javno osvetljenje");

                        Izmeni.ClickOnIt("Izmeni troškovi za javno osvetljenje");

                        break;
                    }

                case "Izveštaji":
                    {
                        Izvestaji.ClickOnIt("Izveštaji");
                        break;
                    }
            };
        }
    }
}

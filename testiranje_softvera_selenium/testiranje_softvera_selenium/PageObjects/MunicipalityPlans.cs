﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using testiranje_softvera_selenium.Extensions;

namespace testiranje_softvera_selenium.PageObjects
{
    public class MunicipalityPlans
    {
        [FindsBy(How = How.CssSelector, Using = ".glyphicon.glyphicon-folder-open")]
        public IWebElement OpenPlanButton { get; set; }

        [FindsBy(How = How.LinkText, Using = "Javno osvetljenje")]
        public IWebElement OpenJavnaPreduzecaButton { get; set; }

        public void ClickToOpenPlanButton()
        {
            OpenPlanButton.ClickOnIt("OpenPlanButton");
            
            OpenJavnaPreduzecaButton.ClickOnIt("OpenJavnaPreduzecaButton");
        }
    }
}
